/*
Autor: Tomás de Carvalho Coelho, Eng comp, 418391
Manipulação de bits
*/

#include "bit.h"
#include <stdio.h>

int main() {
  unsigned long teste = 24;
  printf("%lu\n", teste);
  setBit(&teste, 0);
  printf("%lu\n\n", teste);
  unsigned long slice = 0;
  teste = 7;
  slice = readBitSlice(&teste, 31, 0);
  printf("%lu\n\n", slice);
  readBit(&teste, 2);
  printf("%lu\n", teste);
  return 0;
}
