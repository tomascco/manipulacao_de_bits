
#include "bit.h"

void setBit(unsigned long* p, unsigned char bitn) {
  *p = *p | (1 << bitn);
}

void resetBit(unsigned long* p, unsigned char bitn) {
  *p = *p & ( ~(1 << bitn) );
}

void toggletBit(unsigned long* p, unsigned char bitn) {
  *p = *p ^ (1 << bitn);
}

unsigned char readBit(unsigned long* p, unsigned char bitn) {
  return (*p >> bitn) & 1;
}

unsigned long readBitSlice(unsigned long* p, unsigned char bitstart, unsigned char bitend) {
  unsigned long mask = ( (1 << ( bitend - bitstart + 1 ) ) - 1 );

  return (*p >> bitstart) & mask;
}

unsigned char isLittleEndian() {
  unsigned long teste = 1;
  return (unsigned char) *(&teste);
}
